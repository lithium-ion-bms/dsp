/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* MODULENAME.: SoCTask.c
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
*171120  SQ 	Module Created
*171129  SQ 	Cleanup and Comment
*****************************************************************************/

/***************************** Include files *******************************/

#include "Sysinfo.h"
#include "hw_memmap.h"
#include "GPIO.h"

/*****************************    Defines    *******************************/
/*****************************    Defines    *******************************/
#define dt 				0.005			// The ADC sample frequency will be used as the dt
#define Hour 			0.000277		// Define the multiplier factor to get hour
#define ConvertToMili 	1/1000			// Define Variable to convert to mili

/*****************************   Constants   *******************************/


/*****************************   Constants   *******************************/


/*****************************   Variables   *******************************/

uint16_t MaxCapacity; 	// Variable to store the maximal capacity of the cell.
						// Relative to when the system is at 100%.

fp64_t PrevEstimate;

/*****************************   Functions   *******************************/

// Define prototype.

void Soc_Task(void *pvParameters);
void CoulombCounting();
void CalculateGain();
void CalculateEstimate();
void CalculateErrorEstimate();



void CoulombCounting()
/*****************************************************************************
*   Input    : -
*   Output   : Mili coulomb per hour / mAH.
*   Function : Calculate the mili coulomb per hour.
******************************************************************************/
{
Coulomb.KalmanMeasurement = Current*dt*Hour;
}


void CalculateGain()
/*****************************************************************************
*   Input    : -
*   Output   : The Kalman Gain.
*   Function : Calculate the Kalman gain.
******************************************************************************/
{
	Coulomb.KalmanGain = Coulomb.KalmanErrorEstimate /(Coulomb.KalmanErrorEstimate + Coulomb.KalmanErrorMeasurement);
}

void CalculateEstimate()
/*****************************************************************************
*   Input    : -
*   Output   : Kalman Estimate
*   Function : Calculate the Kalman Estimate
******************************************************************************/
{
	Coulomb.KalmanEstimate = Coulomb.KalmanEstimate + Coulomb.KalmanGain*(Coulomb.KalmanMeasurement - Coulomb.KalmanEstimate);
	Current = Coulomb.KalmanEstimate/(dt*Hour);
}

void CalculateErrorEstimate()
/*****************************************************************************
*   Input    : -
*   Output   : Error in the estimate
*   Function : Calculate the error in the estimate
******************************************************************************/
{
	Coulomb.KalmanErrorEstimate = (1-Coulomb.KalmanGain)*Coulomb.KalmanErrorEstimate;
}


void SoHCalculation()
/*****************************************************************************
*   Input    : -
*   Output   : The State of health of the battery
*   Function : Calculate the state of health of the whole battery module.
******************************************************************************/
{
	StateOfHealth = (MaxCapacity*100) / (2850*6);
}



void Soc_Task(void *pvParameters)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : 	Task to handle the SoC and SoH
*   			calculation for a complete Battery module.
******************************************************************************/
{
	uint16_t Data = 0;
	uint16_t CurrentCapacity;
	fp64_t SoC = StateOfCharge;
	MaxCapacity = 2850*6;  					// MaxCapacity is over the whole module.
											// Receive the CellInfo from the MCU, only when the system start up from cold and when they change, else store them in HibernationData
	Coulomb.KalmanErrorEstimate = 50;
	Coulomb.KalmanErrorMeasurement = 0.5; 	// HW - Resistor tolerance, voltage drop, CT accurcy, ADC error, some time maybe ?
	Coulomb.KalmanEstimate = 0.0000134;
	Coulomb.KalmanMeasurement = 0.0000134;
	Coulomb.KalmanGain = 1;
	CurrentCapacity = Coulomb.KalmanEstimate;

	while(1)
	{
		xQueueReceive(DspEventQueue,&Data,portMAX_DELAY);
		CoulombCounting();
		CalculateGain();
		CalculateEstimate();
		CalculateErrorEstimate();

		// Store the kalman data in the hibernation module. 
		HibernationData[1] = (uint8_t) (Coulomb.KalmanGain*1000000);		
		HibernationData[2] = (uint8_t) (Coulomb.KalmanEstimate*100000);
		HibernationData[3] = (uint8_t) (Coulomb.KalmanErrorEstimate*1000000);
		HibernationData[4] = (uint8_t) (Coulomb.KalmanMeasurement*100000);
		HibernationData[5] = (uint8_t) (Coulomb.KalmanErrorMeasurement);


		if ( (Discharge == TRUE) && (Charge == FALSE) )
		{
			SoC = (SoC-(Coulomb.KalmanEstimate*100)/(MaxCapacity));
			CurrentCapacity -= Coulomb.KalmanEstimate;
		}else if ( (Discharge == FALSE) && (Charge == TRUE))
		{
			SoC = (SoC+(Coulomb.KalmanEstimate*100)/(MaxCapacity));
			CurrentCapacity += Coulomb.KalmanEstimate;
		}

		UARTprintf("SoC = %d\n",(uint16_t)(SoC*100));
		if ( (Voltage > 3600) && (SoC > 100 ))
		{
			SoC = 100;
			MaxCapacity = CurrentCapacity;
		}else if ( Voltage < 2600 )
		{
			SoC = 0;
		}

		SoHCalculation();


		UARTCharPut(UART3_BASE,(uint8_t) SoC);
		UARTCharPut(UART3_BASE,(uint8_t) StateOfHealth);
		UARTCharPut(UART3_BASE,(uint8_t) (Current >> 8));
		UARTCharPut(UART3_BASE,(uint8_t) Current);


		if ( OpMode.HibernationMode == TRUE)
		{
			xQueueSendToBack(HibernationActQueue,&Data,portMAX_DELAY);
		}
	}

}

/****************************** End Of Module *******************************/
