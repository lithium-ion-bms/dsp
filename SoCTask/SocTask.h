/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: SoCTask.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* DISCRIPTION: This module will calculate the SoC and SoH for a battery module.
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
*171120  SQ 	Module Created
*171129  SQ 	Cleanup and Comment
*****************************************************************************/

#ifndef SOCTASK_H_
  #define SOCTASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/


void Soc_Task(void *pvParameters);


/****************************** End Of Module *******************************/
#endif
