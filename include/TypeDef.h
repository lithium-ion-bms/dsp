/*****************************************************************************
* University of Southern Denmark
* Embedded Programming (EMP)
*
* MODULENAME.: emp_type.h
*
* PROJECT....: EMP
*
* DESCRIPTION: Definements of variable types.
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 050128  KA    Module created.
*
*****************************************************************************/

#ifndef TYPEDEF_H_
#define TYPEDEF_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

#define BmsAdcRefVolt 5
#define mV 	1000
#define MiliScale 			BmsAdcRefVolt*mV/65535

typedef enum {
	eOk, 				// Function returned succesfully.
	eCellNumberFailed, 	// Error message for Cell number information, Number of cell does not match number of string.
	eUartBaudFalse,		// The baud rate for UART is not correct.
	eUartConfigFalse,	// The config for the UART is not correct.
	eSemphFault,		// The config for semaphor did not succed.
	eUartBaseFalse		// Base is not correct
} eReturn_t;


typedef enum  {
	TRUE,
	FALSE
} bool_t;


typedef unsigned char  			uint8_t;     /* Unsigned  8 bit quantity              */
typedef signed   char  			int8_t;     /* Signed    8 bit quantity              */
typedef unsigned short   		uint16_t;    /* Unsigned 16 bit quantity              */
typedef signed   short   		int16_t;    /* Signed   16 bit quantity              */
typedef unsigned long  			uint32_t;    /* Unsigned 32 bit quantity              */
typedef signed   long  			int32_t;    /* Signed   32 bit quantity              */
typedef unsigned long long		uint64_t;    /* Unsigned 64 bit quantity              */
typedef signed   long long 		int64_t;    /* Signed   64 bit quantity              */
typedef float          			fp32_t;      /* Single precision floating point       */
typedef double         			fp64_t;      /* Double precision floating point       */


/*****************************   Constants   *******************************/

/*****************************   Functions   *******************************/

/****************************** End Of Module *******************************/
#endif /* TYPEDEF_H_ */
