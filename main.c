/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: main.c
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DESCRIPTION: See module specification file (.h-file).
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171311  SQ    Module created.
* 271311  SQ    Cleanup & added comment
*
*****************************************************************************/

/***************************** Include files *******************************/


#include "FreeRTOS.h"
#include "Task.h"
#include "sysctl.h"
#include "hw_memmap.h"
#include "uart.h"
#include "gpio.h"
#include "uartstdio.h"
#include "buttons.h"

#include "Sysinfo.h"
#include "status_led.h"
#include "SocTask.h"
#include "DspTask.h"
#include "hibernationTask.h"
#include "hibernate.h"

/*****************************    Defines    *******************************/

#define TARGET_IS_TM4C123_RA1 // Define target, for use of API
#define USERTASK_STACK_SIZE configMINIMAL_STACK_SIZE // Set task stack size, set for FreeRTOS
#define IDLE_PRIO 0	// Set different priority
#define LOW_PRIO  1	// Set different priority
#define MED_PRIO  2	// Set different priority
#define HIGH_PRIO 3	// Set different priority

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/

#define BmsBoard		2  // Set default configuration
#define CellPerString 	6  // Set default configuration
#define TempSense 		3  // Set default configuration
#define Cell			BmsBoard*CellPerString   // Set default configuration




/*****************************   Constants   *******************************/


/*****************************   Variables   *******************************/


/*****************************   Functions   *******************************/
// Prototype function.
eReturn_t InitQueue();
void Init();
eReturn_t InitVariables(void);
void setupHardware(void);



eReturn_t InitVariables(void)
/*****************************************************************************
*   Input    :  -
*   Output   : Status of init
*   Function : This function will initialize the system variables.
*****************************************************************************/
{
	eReturn_t eVal;
	eVal = eOk;

	// Initialize Cell info with standard information
	CellInfo.NumberOfCell = Cell;
	CellInfo.NumberOfCellPerString = CellPerString;
	CellInfo.NumberOfBmsBoard = BmsBoard;
	CellInfo.NumberOfTempSense = TempSense;
	CellInfo.BatteryCapacity = 2850;

	// Initialize System information/ Set default state.
	Current = 0;
	StateOfCharge = 50;
	StateOfHealth = 0;
	Discharge = TRUE;
	Charge = FALSE;



	return eVal;
}

void setupHardware(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function : 	This function will initialize the hardware for the needed for
*   			the system.
*****************************************************************************/
{


  // Warning: If you do not initialize the hardware clock, the timings will be inaccurate
  // Set clock to 80MHz
	 SysCtlClockSet(SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ|SYSCTL_SYSDIV_2_5);
  // Init the hardwares
	 ButtonsInit();
	 InitStatus = status_led_init();
	 InitStatus |= InitVariables();
	 InitStatus |= InitUart();
	 InitStatus |= InitSysinfo();
	 InitStatus |= InitQueue();

// FPUEnable();

	/****************************** FPU INIT ******************************/
	asm("LDR.W R0, =0xE000ED88");
	asm("LDR R1, [R0]");
	asm("ORR R1,R1 , #(0xF << 20)");
	asm("STR R1, [R0]");
	asm("DSB");
	asm("ISB");
	/*************************** FPU INIT END *****************************/


}

eReturn_t InitQueue()
/*****************************************************************************
*   Input    :  -
*   Output   :  Status of the initialization process
*   Function : Initialize and create the queue needed for the system.
*****************************************************************************/
{
	DspEventQueue = xQueueCreate( 1, sizeof(uint16_t) );
	xQueueReceive(DspEventQueue,&Current,0);
	HibernationActQueue = xQueueCreate( 1, sizeof(uint16_t) );
	xQueueReceive(HibernationActQueue,&Current,0);
	return eOk;
}


void Init()
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function :  Determine what caused the startup and act accordingly
*****************************************************************************/
{

	uint32_t ui32Status;
	uint8_t Data;
    HibernateGPIORetentionDisable();
	//
	// Need to enable the hibernation peripheral after wake/reset, before using
	// it.
	//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_HIBERNATE);
	//
	// Wait for the Hibernate module to be ready.
	//
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_HIBERNATE))
	{
	}
	//
	// Determine if the Hibernation module is active.
	//
	setupHardware();
	UARTprintf("Determine Startup...\n");
	if(HibernateIsActive())
	{
		//
		// Read the status to determine cause of wake.
		//
		ui32Status = HibernateIntStatus(false);
		//
		// Test the status bits to see the cause.
		//
		if(ui32Status & HIBERNATE_INT_PIN_WAKE)
		{
			UARTprintf("Pin Asserted \n");
		}else if(ui32Status & HIBERNATE_INT_RTC_MATCH_0)
		{
			UARTprintf("RTC interrupt \n");
			InitHibernation();
			ui32Status = GPIOPinRead(GPIO_PORTE_BASE,GPIO_PIN_0);
			GPIOPinWrite(GPIO_PORTF_BASE,(GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 |GPIO_PIN_3 ),(0x00));
			if ( (ui32Status & 0x01) == 0x00 )
			{

				HibernateDataGet( &HibernationData ,7); //Which data points needs to be stored ?
				HibernationData[0] = 0;
				HibernateDataSet( &HibernationData ,7); //Which data points needs to be stored ?
		    	OpMode.HibernationMode = FALSE;
		    	OpMode.StandAloneMode = TRUE;
		    	OpMode.UpsMode = FALSE;
			}else if ( (ui32Status & 0x01) == 0x01 )
			{
				//HibernateRequest();
				//while(1);
				Data = UARTCharGet(UART3_BASE);
				if ( Data == 0xe2 )
				{
					Data = UARTCharGet(UART3_BASE);
					CellInfo.NumberOfCell = Data; //uint8
					Data = UARTCharGet(UART3_BASE);
					CellInfo.NumberOfBmsBoard = Data;  //uint8
					Data = UARTCharGet(UART3_BASE);
					CellInfo.NumberOfCellPerString = Data;  //uint8
					Data = UARTCharGet(UART3_BASE);
					Data = Data << 8;
					Data |= UARTCharGet(UART3_BASE);
					CellInfo.MaximumVoltage = Data;  //uint16
					Data = UARTCharGet(UART3_BASE);
					Data = Data << 8;
					Data |= UARTCharGet(UART3_BASE);
					CellInfo.MinimumVoltage = Data;//uint16
					Data = UARTCharGet(UART3_BASE);
					CellInfo.NumberOfTempSense = Data; //uint8
					Data = UARTCharGet(UART3_BASE);
					Data = Data << 8;
					Data |= UARTCharGet(UART3_BASE);
					CellInfo.BatteryCapacity = Data;//uint16
				}
				//while(UARTCharsAvail(UART3_BASE) == FALSE ){};
				Data = UARTCharGet(UART3_BASE);
				if  ( Data == 0xFF )
				{
					Data = UARTCharGet(UART3_BASE);
					Data = Data << 8;
					Data |= UARTCharGet(UART3_BASE);
					Current = Data;
					Data = UARTCharGet(UART3_BASE);
					Data = Data << 8;
					Data |= UARTCharGet(UART3_BASE);
					Voltage = (Data - 200 );
					xQueueSendToBack(DspEventQueue,&Data,portMAX_DELAY);
				}
			}


		}else if(ui32Status & HIBERNATE_INT_WR_COMPLETE)
		{
			UARTprintf("write complete interrupt\n");
		}else if (ui32Status & HIBERNATE_INT_LOW_BAT)
		{
			UARTprintf("low-battery interrupt\n");
		}else if (ui32Status & HIBERNATE_INT_VDDFAIL)
		{
			UARTprintf("supply failure interrupt\n");
		}else if (ui32Status & HIBERNATE_INT_RESET_WAKE)
		{
			UARTprintf("wake from reset pin interrupt\n");
		}else if (ui32Status & HIBERNATE_INT_GPIO_WAKE)
		{
			UARTprintf("wake from GPIO pin or reset pin interrupt.\n");
		}else
		{
			UARTprintf("Error - Could not determine wakeup\n");
		}



		//
		// Restore program state information that was saved prior to
		// hibernation.
		//
		HibernateDataGet( &HibernationData ,7); //Which data points needs to be stored ?
		//UARTprintf("Hibernation == %d\n",HibernationData);
	    if ( HibernationData[0] == 1 )
	    {
	    	OpMode.HibernationMode = TRUE;
	    	OpMode.StandAloneMode = FALSE;
	    	OpMode.UpsMode = FALSE;
	    }
	    Coulomb.KalmanGain 				= (fp64_t) (HibernationData[1]/1000000);
	    Coulomb.KalmanEstimate 			= (fp64_t) (HibernationData[2]/100000);
	    Coulomb.KalmanErrorEstimate 	= (fp64_t) (HibernationData[3]/1000000);
	    Coulomb.KalmanMeasurement 		= (fp64_t) (HibernationData[4]/1000000);
	    Coulomb.KalmanErrorMeasurement  = (fp64_t) (HibernationData[5]);




		//
		// Now that wake up cause has been determined and state has been
		// restored, the program can proceed with normal processor and
		// peripheral initialization.
		//
	}
	//
	// Hibernation module was not active, so this is a cold power-up/reset.
	//
	else
	{
		//UARTprintf("Cold Start \n");
		//
		// Perform normal power-on initialization.
		//
	}
	InitHibernation();

}


int main(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function : 	Main function, set up the task to be executed,
*   			initialize and run the scheduler.
*****************************************************************************/
{
	// Initialize the system and determine the startup.
	Init();

  /*
   Start the tasks defined within this file/specific to this demo.
   */
  xTaskCreate( status_led_task, ( signed portCHAR * ) "Status_led", USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );
  xTaskCreate( Soc_Task, ( signed portCHAR * ) "SOC", USERTASK_STACK_SIZE, NULL, HIGH_PRIO, NULL );
  xTaskCreate( Dsp_Task, ( signed portCHAR * ) "Dsp", USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );
  xTaskCreate( Hibernation_task, ( signed portCHAR * ) "Hibernation", USERTASK_STACK_SIZE, NULL, LOW_PRIO, NULL );





  /*
   * Start the scheduler.
   */
  vTaskStartScheduler();

  /*
   * Will only get here if there was insufficient memory to create the idle task.
   */
	GPIOPinWrite(GPIO_PORTF_BASE,(GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 |GPIO_PIN_3 ),(0x02));
  return 1;
}
