/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* MODULENAME.: DspTask.c
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171311  SQ    Module created
* 271311  SQ    Cleanup & added comment
*
*****************************************************************************/

/***************************** Include files *******************************/

#include "FreeRTOS.h"
#include "queue.h"
#include "Sysinfo.h"
#include "hw_memmap.h"
#include "uart.h"
#include "UARTstdio.h"

/*****************************    Defines    *******************************/

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/



void Dsp_Task(void *pvParameters)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function : 	This Task communicate with the MCU
*****************************************************************************/
{
	uint16_t Data = 0x20; // Initialize Variables
	while(1)
	{
		Data = UARTCharGet(UART3_BASE); // Get Header data to determine the command.
		if ( (Data == 0xE1)) 			// Hibernation mode is active, encoded in the command.
		{
			OpMode.HibernationMode = TRUE;
		}else if ( Data == 0xE0 )		// Hibernation mode is deactivated, encoded in command
		{
			OpMode.HibernationMode = FALSE;
		}else if ( Data == 0xff )		// Recieve data, command.
		{
			// Recieve data and store them in a uint16_t
			Data = UARTCharGet(UART3_BASE);
			Data = Data << 8;
			Data |= UARTCharGet(UART3_BASE);
			Current = Data;
			Data = UARTCharGet(UART3_BASE);
			Data = Data << 8;
			Data |= UARTCharGet(UART3_BASE);
			Voltage = (Data - 200 );
			xQueueSendToBack(DspEventQueue,&Data,portMAX_DELAY);
		}else if ( Data == 0xE2 )
		{
			//
			Data = UARTCharGet(UART3_BASE);
			CellInfo.NumberOfCell = Data; 			//uint8
			Data = UARTCharGet(UART3_BASE);
			CellInfo.NumberOfBmsBoard = Data;  		//uint8
			Data = UARTCharGet(UART3_BASE);
			CellInfo.NumberOfCellPerString = Data;  //uint8
			Data = UARTCharGet(UART3_BASE);
			Data = Data << 8;
			Data |= UARTCharGet(UART3_BASE);
			CellInfo.MaximumVoltage = Data;  		//uint16
			Data = UARTCharGet(UART3_BASE);
			Data = Data << 8;
			Data |= UARTCharGet(UART3_BASE);
			CellInfo.MinimumVoltage = Data;			//uint16
			Data = UARTCharGet(UART3_BASE);
			CellInfo.NumberOfTempSense = Data; 		//uint8
			Data = UARTCharGet(UART3_BASE);
			Data = Data << 8;
			Data |= UARTCharGet(UART3_BASE);
			CellInfo.BatteryCapacity = Data;		//uint16
		}else if ( Data == 0xE3)
		{
				Discharge = FALSE;
				Charge = TRUE;

		}else if ( Data == 0xE4)
		{
				Discharge = TRUE;
				Charge = FALSE;
		}
	}
}
