/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* MODULENAME.: 
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* DISCRIPTION: This module interact with the MCU. Get Current and start the kalman filter.
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171311  SQ    Module created.
* 271311  SQ    Cleanup & added comment
*
*****************************************************************************/

#ifndef DSPTASK_H_
  #define DSPTASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

eReturn_t InitI2c();
void I2cTransmit(uint8_t *pData);
uint32_t I2cRecieve();
void Dsp_Task(void *pvParameters);


/****************************** End Of Module *******************************/
#endif
