/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: HibernationTask.c
*
* PROJECT....:  MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171122  SQ 	Document Created
* 171129  SQ 	Added Comment and cleanup
*
*****************************************************************************/

/***************************** Include files *******************************/

#include "TypeDef.h"
#include "Sysinfo.h"
#include "hibernate.h"
#include "sysctl.h"
#include "GPIO.h"
#include "hw_memmap.h"

/*****************************    Defines    *******************************/
#define HibernationTimeSec 	5
#define HibernationRTCInit	0

/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/


eReturn_t InitHibernation()
/*****************************************************************************
*   Input    : -
*   Output   : Status of the initialization.
*   Function : Initialize and setup of the hibernation module.
******************************************************************************/
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)){};
	GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_0);
	GPIODirModeSet(GPIO_PORTE_BASE, GPIO_PIN_0, GPIO_DIR_MODE_IN);
    GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_0, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);

    HibernateEnableExpClk(SysCtlClockGet());
    HibernateGPIORetentionEnable();
   	HibernateRTCSet(0);
	HibernateRTCEnable();
   	HibernateRTCMatchSet(0, HibernateRTCGet() + 3);
    HibernateWakeSet(HIBERNATE_WAKE_RTC);


    return eOk;
}

void Hibernation_task(void *pvParameters)
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : 	Task to handle Hibernation mode,
*   			only execute when Hibernation mode is active
******************************************************************************/
{
	uint32_t Buffer = 0;

	while(1)
	{
		xQueueReceive(HibernationActQueue,&Buffer,portMAX_DELAY);

		if ( OpMode.HibernationMode == TRUE )
		{
			GPIOPinWrite(GPIO_PORTF_BASE,(GPIO_PIN_1 | GPIO_PIN_2 |GPIO_PIN_3 ),(0x00));
			HibernationData[0] = 1;
		    HibernateDataSet(&HibernationData, 7);		// Store Hibernation mode in the memory
		    vTaskDelay(7 / portTICK_RATE_MS); 			// wait 7 ms.
			if ( UARTCharsAvail(UART3_BASE) == TRUE )
			{
			    vTaskDelay(7 / portTICK_RATE_MS); 		// wait 7 ms.
			}
			HibernateRequest();

			while(1){};
		}

	}

}

/****************************** End Of Module *******************************/
