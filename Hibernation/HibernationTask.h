/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: 
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM
*
* DISCRIPTION: 	The hibernation module, setup and enable the Hibernation
* 				on the DSP.
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 171122  SQ 	Document Created
* 171129  SQ 	Added Comment and cleanup
*
*****************************************************************************/

#ifndef HIBERNATIONTASK_H_
  #define HIBERNATIONTASK_H_

/***************************** Include files *******************************/

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/

/*****************************   Constants   *******************************/

/*************************  Function interfaces ****************************/

eReturn_t InitHibernation();
void Hibernation_task(void *pvParameters);

/****************************** End Of Module *******************************/
#endif
