/*****************************************************************************
* Thesis MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* MODULENAME.: Sysinfo
*
* PROJECT....: MODULAR LITHIUM BATTERY MANAGEMENT SYSTEM 
*
* DISCRIPTION: This module contain all the global variables.
*
*
* Change Log:
*****************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 170730  SQ 	Module Created
*
*****************************************************************************/

#ifndef Sysinfo_H_
  #define Sysinfo_H_

/***************************** Include files *******************************/
#include "TypeDef.h"
#include "FreeRTOS.h"
#include "semphr.h"

/*****************************    Defines    *******************************/

/********************** External declaration of Variables ******************/


// Struct containing information the number of cells in the physical setup.
typedef struct{
	uint8_t NumberOfCell;
	uint8_t NumberOfBmsBoard;
	uint8_t NumberOfCellPerString;
	uint16_t MaximumVoltage; // Measured in mV
	uint16_t MinimumVoltage;
	uint8_t NumberOfTempSense;
	uint16_t BatteryCapacity;
} ui8CellNumber_t;

// Struct for defining the different operation mode of the battery system.
typedef struct {
	bool_t HibernationMode;
	bool_t UpsMode;
	bool_t StandAloneMode;
} bOpMode_t;


typedef struct
{
	fp64_t KalmanGain;
	fp64_t KalmanEstimate;
	fp64_t KalmanErrorEstimate;
	fp64_t KalmanMeasurement;
	fp64_t KalmanErrorMeasurement;

}fp32_KalmanFilter;


/*****************************   Constants   *******************************/


eReturn_t InitStatus;

xQueueHandle DspEventQueue;
xQueueHandle HibernationActQueue;




/*****************************   Variables   *******************************/

ui8CellNumber_t CellInfo;
bOpMode_t OpMode;
fp32_KalmanFilter Coulomb;

uint16_t Voltage;
uint16_t Current;
fp64_t StateOfCharge;
fp64_t StateOfHealth;
uint32_t HibernationData[32];
bool_t Discharge;
bool_t Charge;

/*************************  Function interfaces ****************************/

eReturn_t InitSysinfo(void);
/*****************************************************************************
*   Input    : -
*   Output   : -
*   Function : Init System information
******************************************************************************/

/****************************** End Of Module *******************************/
#endif
